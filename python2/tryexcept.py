#!/usr/bin/env python3

#You might notice in the previous example, that if you enter some text that
#doesn't correspond to an integer, the program exits with an error. Python
#gives you a nice way to try to handle this within your code using the
#[`try`](https://docs.python.org/3/reference/compound_stmts.html#try)
#statement.

val = input("Enter an integer: ")
try:
    # This block contains the command to try.
    a = int(val)
    # val has been read as a string so we try to convert it to an integer.
except:
    # If code "try" block generates an error, this block is executed.
    print(val, "could not be converted to an integer.")
else:
    # If the code in the "try" block succeeds, this block is executed.
    if a > 10:
        print(a, "is greater than 10.")
    else:
        print(a, " is not greater than 10.")
