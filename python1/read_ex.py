#!/usr/bin/env python3

f = open("test.txt")

# This assigns the entire contents of the file to "fullfile"
fullfile = f.read()
print("The file contents are:")
print(fullfile)
print()

# As we read a file, the current position is tracked. After reading the whole
# file, we are positioned at the end, and we need to return to the start if
# we want to re-read the file. We can do this using the seek() method.
f.seek(0)
print("Outputting one line at a time:")
for line in f:
    # We add end='' to suppress the default newline that's output at the end
    # of the print function. There's already a newline at the end of each line
    # of the file.
    print(line, end='')

# And we should make a habit of closing the file once we've finished.
f.close()
