#!/usr/bin/env python3

n = int(input("Enter a positive integer: "))

# We could use while to ensure input fits a certain criteria.
while n <= 0:
    print(n, "is not a positive integer.")
    n = int(input("Enter a positive integer: "))

factorial_n = 1
ii = 1
# Now lets use a while loop to calculate n!.
while ii <= n:
    # We can have the same variable appearing on both the left and right of
    # an assignment. The right hand side is evaluated before the assignment.
    factorial_n = factorial_n * ii
    ii = ii + 1
print(n, "! = ", factorial_n, sep='')
