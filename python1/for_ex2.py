#!/usr/bin/env python3

items = [1, "a", 3, "apple"]
# range(4) produces the numbers 0, 1, 2, 3, so they can be used directly as
# indices of compound types such as strings or lists.
for ii in range(len(items)):
    print("Element", ii, "is", items[ii])

n = int(input("Enter a positive integer: "))
# Now lets use a for loop to calculate n!.
factorial_n = 1
for ii in range(1, n+1):
    factorial_n = factorial_n * ii
print(n, "! = ", factorial_n, sep='')
