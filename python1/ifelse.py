#!/usr/bin/env python3

a = int(input("Enter an integer: "))
# input() returns a string so we convert it to an int.

if a > 10:
    print(a, "is greater than 10.")
elif a == 10:
    print(a, "is equal to 10.")
else:
    print(a, "is less than 10.")
