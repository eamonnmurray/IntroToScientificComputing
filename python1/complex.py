#!/usr/bin/env python3

a = 1.0 + 3.0j
# We could also write "a = complex(1.0, 3.0)"
b = a**0.5

# We remove the default space between arguments in this print() with sep=''.
print("a, b: ", a, ", ", b, sep='')
print("Square root of b:", b**2)
c = b.real # We could use an intermediate variable if we wanted.
print("Real part of b:", c)
print("Imaginary part of a:", a.imag)
c = a.conjugate()
print("Complex conjugate of a:", c)
