Introduction to Scientific Computing
====================================

This is a collection of various notes and examples I've generated over the
past few years, that will hopefully be of use to students as supplementary
material to their courses.

I periodically add corrections and new content and sections. To ensure you
have the latest version:

- Go to <https://gitlab.com/eamonnmurray/IntroToScientificComputing>
- To download everything with git:
  `git clone https://gitlab.com/eamonn.murray/IntroToScientificComputing.git`
    - If you've cloned the repository previously and want to ensure you have
      the most recent updates type `git pull` within the repository directory.

Contents
--------

There are currently sections on the following topics:

- [Computer Systems](systems/README.md)
    - [Binary, Bits and Bytes](systems/README.md#binary-bits-and-bytes)
    - [Components](systems/README.md#components)
    - [High-Performance Computer Systems](systems/README.md#high-performance-computer-systems)
    - [Internal Representation of Data](systems/README.md#internal-representation-of-data)
- [Linux - 1. The Command line](linux1/README.md)
    - [Using the Terminal](linux1/README.md#using-the-terminal)
    - [Tab Completion](linux1/README.md#tab-completion)
    - [Permissions](linux1/README.md#permissions)
    - [Reading Files](linux1/README.md#reading-files)
    - [The Manual and man Command](linux1/README.md#the-manual-and-man-command)
    - [Wildcards](linux1/README.md#wildcards)
    - [IO Redirection](linux1/README.md#io-redirection)
    - [History](linux1/README.md#history)
    - [Other Useful Commands](linux1/README.md#other-useful-commands)
- [Linux - 2. More Advanced Use](linux2/README.md)
    - [Editing Files](linux2/README.md#editing-files)
    - [The Bash Shell](linux2/README.md#bash-shell)
    - [Remote Access](linux2/README.md#remote-access)
- [Linux - 3. Useful Tools](linux3/README.md)
    - [Version Control with Git](linux3/README.md#version-control-with-git)
    - [Plotting](linux3/README.md#plotting)
    - [Visualizing Atomic Structures](linux3/README.md#visualizing-atomic-structures)
    - [Document Preparation with Latex](linux3/README.md#document-prepartion-with-latex)
- [Python - 1. The Basics](python1/README.md)
    - [Installing Python](python1/README.md#installing-python)
    - [Using Python](python1/README.md#using-python)
    - [Hello World - Output](python1/README.md#hello-world-output)
    - [Variables, Strings and IO](python1/README.md#variables-strings-and-IO)
    - [Input](python1/README.md#input)
    - [Basic Types](python1/README.md#basic-types)
    - [Mathematical Operations](python1/README.md#mathematical-operations)
    - [Compound Assignments](python1/README.md#compound-assignments)
    - [More Advanced Math - Python Packages](python1/README.md#more-advanced-math-python-packages)
    - [Branching - if statements](python1/README.md#branching-if-statements)
    - [Python Data Structures - Strings and Lists](python1/README.md#python-data-structures-strings-and-lists)
    - [Loops - while and for statements](python1/README.md#loops-while-and-for-statements)
    - [More Compound Types - sets, tuples and dicts](python1/README.md#more-compound-types-sets-tuples-and-dicts)
    - [Functions](python1/README.md#functions)
    - [Modules](python1/README.md#modules)
    - [Coding Style](python1/README.md#coding-style)
    - [Working with Files](python1/README.md#working-with-files)
- [Python - 2. More Advanced Topics](python2/README.md)
    - [List Comprehensions](python2/README.md#list-comprehensions)
    - [Scope](python2/README.md#scope)
    - [Classes](python2/README.md#classes)
    - [Using Arguments in your Python Script](python2/README.md#using-arguments-in-your-python-script)
    - [Installing Additional Packages](python2/README.md#installing-additional-packages)
    - [Virtual Environments](python2/README.md#virtual-environments)
    - [Catching Exceptions](python2/README.md#catching-exceptions)
    - [Assert](python2/README.md#assert)
    - [Gotchas](python2/README.md#gotchas)
- [Jupyter](jupyter/README.md)
    - [Installation](jupyter/README.md#installation)
    - [Starting Jupyter](jupyter/README.md#starting-jupyter)
    - [Shortcuts and Cell Types](jupyter/README.md#shortcuts-and-cell-types)
    - [Full List of Shortcuts](jupyter/README.md#full-list-of-shortcuts)
    - [Magics](jupyter/README.md#magics)
- [Numpy](numpy/README.md)
    - [Installation](numpy/README.md#installation)
    - [Basic Usage](numpy/README.md#basic-usage)
    - [A Simple Example](numpy/README.md#a-simple-example)
    - [Comparison with Built-In Python](numpy/README.md#comparison-with-built-in-python)
    - [Useful Numpy Operations](numpy/README.md#useful-numpy-operations)
- [Matplotlib](matplotlib/README.md)
    - [Installation](matplotlib/README.md#installation)
    - [A First Example with Pyplot](matplotlib/README.md#a-first-example-with-pyplot)
    - [A More Complex Example](matplotlib/README.md#a-more-complex-example)
    - [Further Reading](matplotlib/README.md#further-reading)
- [Numerical Methods](numerical_methods/README.md)
    - [Root Finding](numerical_methods/01_Root_Finding)
    - [Systems of Equations](numerical_methods/02_Systems_of_Equations)
    - [Interpolation](numerical_methods/03_Interpolation)
    - [Integration](numerical_methods/04_Integration)
    - [Matrix Diagonalization](numerical_methods/05_Matrix_Diagonalization)
    - [Fitting](numerical_methods/06_Fitting)
    - [FFT](numerical_methods/07_FFT)
    - [Differential Equations](numerical_methods/08_Differential_Equations)
    - [Machine Learning](numerical_methods/09_Machine_Learning)

In addition to the material collected here, there are also separate
repositories with material for:

- [Intro to C++](https://gitlab.com/eamonn.murray/IntroToCplusplus). This also
  includes a brief section on Makefiles in the first section.
- [Intro to Mathematica](https://gitlab.com/eamonn.murray/IntroToMathematica)
- The content that was previously in [Intro to Linux](https://gitlab.com/eamonn.murray/IntroToLinux)
  has been incorporated into this repository.

Additional Resources
--------------------

### Linux

- There are various useful
  [guides at the Linux Documentation Project](http://www.tldp.org/guides.html),
  in particular:
    - [Introduction to Linux](http://www.tldp.org/LDP/intro-linux/html/index.html)
    - [Bash Guide for Beginners](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/index.html)

### HPC and Scientific Computing

- Once you want to start thinking about more advanced scientific computing,
  Victor Eijkhout's free textbook
  [Introduction to High-Performance Scientific Computing](http://pages.tacc.utexas.edu/~eijkhout/istc/istc.html)
  is an excellent resource.

### Python

- There are many excellent free resources available online, particularly at the
  official python website:
    - The python wiki: <https://wiki.python.org>. This links to beginner
      guides and has other useful information such as recommended books.
    - There is also a tutorial at
      <https://docs.python.org/3/tutorial/index.html>.
    - The official python documentation website:
      <https://www.python.org/doc/>. This is particularly useful for finding
      how particular functionality should work in the version of python you
      have. Throughout the course, when a function is mentioned for the first
      time, it will be linked to its official documentation page. Many
      functions have options beyond what we will have time to cover so it's
      worth checking these to get a better idea of what you can do.
- There are also many good books out there, in particular:
    - Al Sweigart, _Automate the Boring Stuff with Python_, No Starch Press
      (2015). This is a great introduction to Python language with a focus on
      learning how to put python to work doing useful things for you. The
      material in the book is free to read online at
      <https://automatetheboringstuff.com/>.
    - Christian Hill, _Learning Scientific Programming with Python_, Cambridge
      University Press (2016). This is an excellent companion to the course
      and will give you an alternative view on many of the topics covered
      here.
